/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm;

import java.util.List;
import javafx.application.Application;

import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import repositoriojm.controller.GestorDeArchivosController;
import repositoriojm.util.AppContext;
import repositoriojm.util.TbxControl;

/**
 *
 * @author LordLalo
 */
public class RepositorioJM extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        TbxControl.getInstance().startControl(stage, null, null);
        TbxControl.getInstance().viewBase(true, null);
        TbxControl.getInstance().view("Login");
        
        
        //evento al cerrar el programa por la quis
        stage.setOnCloseRequest((WindowEvent we) -> {

            GestorDeArchivosController con = (GestorDeArchivosController) AppContext.getInstance().get("GestorDeArchivosController");
            if (con!=null) {
                if (con.getEsteSi()) {
                    con.commitForzado();                    
                }
            }
                    
           
            
        });

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
