/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.services;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import repositoriojm.model.Directorio;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.Permiso;
import repositoriojm.model.PermisoDto;
import repositoriojm.model.Usuario;
import repositoriojm.model.UsuarioDto;
import repositoriojm.util.EntityManagerHelper;
import repositoriojm.util.Respuesta;

/**
 *
 * @author roberth :)
 */
public class UsuarioService {

    @SuppressWarnings({"static-access", "FieldMayBeFinal"})
    private EntityManager em = EntityManagerHelper.getInstance().getManager();
    private EntityTransaction et;
    private Usuario usuario;

    public Respuesta getUsuario(String user, String password) {
        try {
            Query search = em.createNamedQuery("Usuario.obtenerPorCredenciales", Usuario.class);
            search.setParameter("user", user);
            search.setParameter("pass", password);
            Usuario resutado = (Usuario) search.getSingleResult();
            UsuarioDto usuarioDto = new UsuarioDto(resutado);

            if (resutado.getUserId() == 1) {  
                usuarioDto.setAdministrador(true);
            }
                for (Permiso t : resutado.getPermisoList()) {
                    PermisoDto permisoDto = new PermisoDto(t);
                    usuarioDto.getPermisos().add(permisoDto);
                    DirectorioDto directorioDto = new DirectorioDto(t.getDirid());
                    usuarioDto.getDirectorios().add(directorioDto);
                }

                
         
            return new Respuesta(true, "", "", "UsuarioDto", usuarioDto);

        } catch (NonUniqueResultException UEX) {
            java.util.logging.Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Buscando usuario por credenciales", UEX);
            return new Respuesta(false, "Ha habido un error al iniciar sesión", "Hubo más de un resultado");
        } catch (NoResultException NR) {
            java.util.logging.Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Buscando usuario por credenciales", NR);
            return new Respuesta(false, "Usuario o contraseña incorrectos", "No han habido resultados");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Buscando usuario por credenciales", ex);
            return new Respuesta(false, "Error desconocido, busca ayuda profesional.", "Ya Ses pi... Tere :v");
        }
    }

    public Respuesta guardar_ModificarUsario(UsuarioDto user) {
        try {
            et = em.getTransaction();
            et.begin();
            if (user.getId() == null) {
                usuario = new Usuario(user);
                usuario.setUserId(this.getNextId());
                em.persist(usuario);
                List<Directorio> directorioList = new ArrayList<>();
                List<Permiso> permisoList = new ArrayList<>();

                for (int i = 0; i < user.getDirectorios().size(); i++) {

                    Directorio dir = new Directorio(user.getDirectorios().get(i));
                    dir.setDirid(this.getNextIdDirectorios());
                    em.persist(dir);
                    directorioList.add(dir);
                    
                    Permiso per1 = new Permiso(user.getPermisos().get(i));
                    per1.setPerId(this.getNextIdPermisos());
                    em.persist(per1);
                    per1.setUserId(usuario);

                    per1.setDirid(dir);
                    permisoList.add(per1);

                    usuario.getPermisoList().add(per1);
                }

            } else {
                usuario = em.find(Usuario.class, user.getId());

                for (int i = 0; i < user.getDirectorios().size(); i++) {

                    Directorio dir = new Directorio(user.getDirectorios().get(i));
                    if (user.getDirectorios().get(i).getDirid() == null) {
                        dir.setDirid(this.getNextIdDirectorios());
                        em.persist(dir);
                    }
                    if (user.getPermisos().size()==user.getDirectorios().size()) {

                        if (user.getPermisos().get(i).getPerId() == null) {
                            Permiso per1 = new Permiso(user.getPermisos().get(i));
                            per1.setPerId(this.getNextIdPermisos());

                            per1.setUserId(usuario);
                            per1.setDirid(dir);
                            em.persist(per1);
                            usuario.getPermisoList().add(per1);
                        }
                    }

                }

                usuario.actualizar(user);
                em.merge(usuario);
            }
            em.flush();
            et.commit();
            return new Respuesta(true, "", "", "data", new UsuarioDto(usuario));
        } catch (Exception ex) {
            et.rollback();
            java.util.logging.Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Guardando o actualizando usuario", ex);
            return new Respuesta(false, "No ha sido posible modificar o actualizar usuario.", "Algo se jodio aqui :(");
        }
    }

    public Respuesta getUsuarioList() {

        try {
            Query queryUsuario = em.createNamedQuery("Usuario.findAll", Usuario.class);
            List<Usuario> usuarios = queryUsuario.getResultList();
            ObservableList<UsuarioDto> usuariosDto = FXCollections.observableArrayList();
            usuarios.forEach((usuario) -> {
                UsuarioDto usuarioDto = new UsuarioDto(usuario);
                usuariosDto.add(usuarioDto);
            });
            return new Respuesta(true, "", "", "UsuariosList", usuariosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, "No existe una empresa con el nombre ingresado.", "getEmpresa NoResultException");
        } catch (NonUniqueResultException ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la empresaa.", "getEmpresa NonUniqueResultException");
        } catch (Exception ex) {
            Logger.getLogger(UsuarioService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar la empresa.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la empresa.", "getEmpresa " + ex.getMessage());
        }
    }

    private int getNextId() {
        Query getId = em.createNamedQuery("Usuario.getMaxId", Usuario.class);
        Integer id = (Integer) getId.getSingleResult();
        return id + 1;
    }

    private int getNextIdPermisos() {
        Query getId = em.createNamedQuery("Permiso.getMaxId", Permiso.class);
        Integer id = (Integer) getId.getSingleResult();
        return id + 1;
    }

    private int getNextIdDirectorios() {
        Query getId = em.createNamedQuery("Directorio.getMaxId", Permiso.class);
        Integer id = (Integer) getId.getSingleResult();
        return id + 1;
    }
}
