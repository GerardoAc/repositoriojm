/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.model;

/**
 *
 * @author jocse
 */
public class PermisoDto {

    private Integer perId;
    private String permtipo;
    private Directorio dirid;
    private Usuario userId;

    public PermisoDto(Permiso permiso) {
        this.perId = permiso.getPerId();
        this.permtipo = permiso.getPermtipo();
        this.dirid = permiso.getDirid();
        this.userId = permiso.getUserId();
    }

    public PermisoDto() {
    }


    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public String getPermtipo() {
        return permtipo;
    }

    public void setPermtipo(String permtipo) {
        this.permtipo = permtipo;
    }

    public Directorio getDirid() {
        return dirid;
    }

    public void setDirid(Directorio dirid) {
        this.dirid = dirid;
    }

    public Usuario getUserId() {
        return userId;
    }

    public void setUserId(Usuario userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return  permtipo;
    }

}
