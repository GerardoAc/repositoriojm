/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.model;

import com.jfoenix.controls.JFXButton;
import java.util.List;

/**
 *
 * @author jocse
 */
public class DirectorioDto extends JFXButton{

    private Integer dirid;
    private String dirLocation;
    private List<PermisoDto> permisoList;

    public DirectorioDto() {
    }

    public DirectorioDto(Directorio directorio) {
        this.dirid = directorio.getDirid();
        this.dirLocation = directorio.getDirLocation();
    }

    public Integer getDirid() {
        return dirid;
    }

    public void setDirid(Integer dirid) {
        this.dirid = dirid;
    }

    public String getDirLocation() {
        return dirLocation;
    }

    public void setDirLocation(String dirLocation) {
        this.dirLocation = dirLocation;
    }

    public List<PermisoDto> getPermisoList() {
        return permisoList;
    }

    public void setPermisoList(List<PermisoDto> permisoList) {
        this.permisoList = permisoList;
    }

    

}
