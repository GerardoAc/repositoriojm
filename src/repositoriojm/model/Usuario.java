/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author rober
 */
@Entity
@Table(name = "Usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByUserId", query = "SELECT u FROM Usuario u WHERE u.userId = :userId")
    , @NamedQuery(name = "Usuario.findByUserNombre", query = "SELECT u FROM Usuario u WHERE u.userNombre = :userNombre")
    , @NamedQuery(name = "Usuario.findByUserUsuario", query = "SELECT u FROM Usuario u WHERE u.userUsuario = :userUsuario")
    , @NamedQuery(name = "Usuario.findByUserPassword", query = "SELECT u FROM Usuario u WHERE u.userPassword = :userPassword")
    , @NamedQuery(name = "Usuario.getMaxId", query = "SELECT MAX(u.userId) FROM Usuario u")
    , @NamedQuery(name = "Usuario.obtenerPorCredenciales", query = "SELECT u FROM Usuario u WHERE u.userUsuario = :user AND u.userPassword = :pass")})
public class Usuario implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name = "User_Id")
    private Integer userId;
    @Column(name = "User_Nombre")
    private String userNombre;
    @Basic(optional = false)
    @Column(name = "User_Usuario")
    private String userUsuario;
    @Basic(optional = false)
    @Column(name = "User_Password")
    private String userPassword;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "userId", fetch = FetchType.LAZY)
    
    private List<Permiso> permisoList = new ArrayList<>();

    public Usuario() {
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Usuario(UsuarioDto user) {
        this.userId = user.getId();
        actualizar(user); 
    }

    public void actualizar(UsuarioDto user) {
        this.userNombre = user.getNombre();
        this.userUsuario = user.getUsuario();
        this.userPassword = user.getContrasenna();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserNombre() {
        return userNombre;
    }

    public void setUserNombre(String userNombre) {
        this.userNombre = userNombre;
    }

    public String getUserUsuario() {
        return userUsuario;
    }

    public void setUserUsuario(String userUsuario) {
        this.userUsuario = userUsuario;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @XmlTransient
    public List<Permiso> getPermisoList() {
        return permisoList;
    }

    public void setPermisoList(List<Permiso> permisoList) {
        this.permisoList = permisoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "repositoriojm.model.Usuario[ usuarioPK=" + userId + " ]";
    }

}
