/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author roberth :)
 */
public class UsuarioDto {

    private Integer id;
    private String nombre;
    private String usuario;
    private String contrasenna;
    private List<PermisoDto> permisos = new ArrayList<>();
    private List<DirectorioDto> directorios = new ArrayList<>();
    private Boolean administrador = false;

    public UsuarioDto() {
    }
    
    
    public UsuarioDto(Usuario entityUser) {
        this.nombre = entityUser.getUserNombre();
        this.usuario = entityUser.getUserUsuario();
        this.contrasenna = entityUser.getUserPassword();
        this.id = entityUser.getUserId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public List<PermisoDto> getPermisos() {
        return permisos;
    }

    public void setPermisos(List<PermisoDto> permisos) {
        this.permisos = permisos;
    }

    public List<DirectorioDto> getDirectorios() {
        return directorios;
    }

    public void setDirectorios(List<DirectorioDto> directorios) {
        this.directorios = directorios;
    }

    public Boolean getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Boolean administrador) {
        this.administrador = administrador;
    }
    
}
