/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rober
 */
@Entity
@Table(name = "Permisos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Permiso.findAll", query = "SELECT p FROM Permiso p")
    , @NamedQuery(name = "Permiso.findByPerId", query = "SELECT p FROM Permiso p WHERE p.perId = :perId")
    , @NamedQuery(name = "Permiso.getMaxId", query = "SELECT MAX(u.perId) FROM Permiso u")
    , @NamedQuery(name = "Permiso.findByPermtipo", query = "SELECT p FROM Permiso p WHERE p.permtipo = :permtipo")})
public class Permiso implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name = "Per_Id")
    private Integer perId;
    @Basic(optional = false)
    @Column(name = "Perm_tipo")
    private String permtipo;
    @JoinColumn(name = "Dir_id", referencedColumnName = "Dir_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Directorio dirid;
    @JoinColumn(name = "User_Id", referencedColumnName = "User_Id")
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Usuario userId;

    public Permiso() {
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Permiso(PermisoDto per) {
        this.perId = per.getPerId();
        actualizar(per);
    }

    public void actualizar(PermisoDto per) {
        this.permtipo = per.getPermtipo();
    }

    public int getPerId() {
        return perId;
    }

    public void setPerId(int perId) {
        this.perId = perId;
    }

    public String getPermtipo() {
        return permtipo;
    }

    public void setPermtipo(String permtipo) {
        this.permtipo = permtipo;
    }

    public Directorio getDirid() {
        return dirid;
    }

    public void setDirid(Directorio dirid) {
        this.dirid = dirid;
    }

    public Usuario getUserId() {
        return userId;
    }

    public void setUserId(Usuario userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perId != null ? perId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permiso)) {
            return false;
        }
        Permiso other = (Permiso) object;
        if ((this.perId == null && other.perId != null) || (this.perId != null && !this.perId.equals(other.perId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "repositoriojm.model.Permiso[ permisoPK=" + perId + " ]";
    }

}
