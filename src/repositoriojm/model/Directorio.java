/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roberth :)
 */
@Entity
@Table(name = "Directorios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Directorio.findAll", query = "SELECT d FROM Directorio d")
    , @NamedQuery(name = "Directorio.findByDirid", query = "SELECT d FROM Directorio d WHERE d.dirid = :dirid")
     , @NamedQuery(name = "Directorio.getMaxId", query = "SELECT MAX(u.dirid) FROM Directorio u")
    , @NamedQuery(name = "Directorio.findByDirLocation", query = "SELECT d FROM Directorio d WHERE d.dirLocation = :dirLocation")})
public class Directorio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Dir_id")
    private Integer dirid;
    @Basic(optional = false)
    @Column(name = "Dir_Location")
    private String dirLocation;
    @OneToMany(mappedBy = "dirid", fetch = FetchType.LAZY)
    private List<Permiso> permisoList;

    public Directorio() {
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Directorio(DirectorioDto dir) {
        this.dirid = dir.getDirid();
        actualizar(dir); 
    }

    public void actualizar(DirectorioDto dir) {
        this.dirLocation = dir.getDirLocation();
    }

    
    
    
    public int getDirid() {
        return dirid;
    }

    public void setDirid(int dirid) {
        this.dirid = dirid;
    }

    public String getDirLocation() {
        return dirLocation;
    }

    public void setDirLocation(String dirLocation) {
        this.dirLocation = dirLocation;
    }

    @XmlTransient
    public List<Permiso> getPermisoList() {
        return permisoList;
    }

    public void setPermisoList(List<Permiso> permisoList) {
        this.permisoList = permisoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
         hash += dirid != null ? dirid : 0;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Directorio)) {
            return false;
        }
        Directorio other = (Directorio) object;
        if ((this.dirid == null && other.dirid != null) || (this.dirid != null && !this.dirid.equals(other.dirid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "repositoriojm.model.Directorio[ directorioPK=" + dirid + " ]";
    }
    
}
