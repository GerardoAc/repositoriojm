/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.PermisoDto;
import repositoriojm.model.UsuarioDto;
import repositoriojm.services.UsuarioService;
import repositoriojm.util.AppContext;
import repositoriojm.util.MensajePopUp;
import repositoriojm.util.Respuesta;
import repositoriojm.util.TbxControl;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class DarPermisoController extends Rechargeable implements Initializable {

    @FXML
    private JFXComboBox<PermisoDto> cmbPermisos;

    private PermisoDto per;

    private UsuarioDto usu;

    private DirectorioDto dir;

    private ObservableList<PermisoDto> permisosDto = FXCollections.observableArrayList();

    ;
    @FXML
    private JFXButton btnGuardado;
    @FXML
    private JFXButton btnVolver;

    private MensajePopUp msjP = new MensajePopUp();
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtUsuario;

    private Boolean inicio = true;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usu = (UsuarioDto) AppContext.getInstance().get("Usuario");
        dir = (DirectorioDto) AppContext.getInstance().get("Directorio");

    }

    @Override
    public void reOpen() {
        usu = (UsuarioDto) AppContext.getInstance().get("Usuario");
        dir = (DirectorioDto) AppContext.getInstance().get("Directorio");
        if (inicio) {
            inicio = false;
            cargarComboBox();
        }

    }

    private void cargarComboBox() {
        txtNombre.setText(usu.getNombre());
        txtUsuario.setText(usu.getUsuario());
        PermisoDto permiso1 = new PermisoDto();
        permiso1.setPermtipo("Total-otro");
        permisosDto.add(permiso1);
        PermisoDto permiso2 = new PermisoDto();
        permiso2.setPermtipo("Medio-otro");
        permisosDto.add(permiso2);
        PermisoDto permiso3 = new PermisoDto();
        permiso3.setPermtipo("Ninguno-otro");
        permisosDto.add(permiso3);
        cmbPermisos.setItems(permisosDto);
        eventoSeleccion();
    }

    private void eventoSeleccion() {
        cmbPermisos.setOnMouseClicked((event) -> {
            per = cmbPermisos.getSelectionModel().getSelectedItem();
        });

    }

    @FXML
    private void guardar(ActionEvent event) {

        if (per != null) {
            usu.getPermisos().add(per);
            usu.getDirectorios().add(dir);
            registro();
        } else {
            msjP.notifyMensajeError("Falta", "Se debe seleccionar un permiso");
        }
    }

    private void registro() {
        try {
            UsuarioService usuarioService = new UsuarioService();
            Respuesta respuesta = usuarioService.guardar_ModificarUsario(usu);

            if (respuesta.getEstado()) {
                msjP.notifyMensajeInformacion("Registor", "El registro fue exitoso");
                TbxControl.getInstance().eliminarLoader("DarPermiso");
                ((Stage) btnVolver.getScene().getWindow()).close();
            } else {
                msjP.notifyMensajeError("Registor", respuesta.getMensaje());
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }
    }

    @FXML
    private void volver(ActionEvent event) {
        TbxControl.getInstance().eliminarLoader("DarPermiso");
        ((Stage) btnVolver.getScene().getWindow()).close();
    }

}
