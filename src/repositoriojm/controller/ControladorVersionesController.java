/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.UsuarioDto;
import repositoriojm.util.AppContext;
import repositoriojm.util.ControlDirectorios;
import repositoriojm.util.TbxControl;
import repositoriojm.util.Transicion;

/**
 * FXML Controller class
 *
 * @author rober
 */
public class ControladorVersionesController extends Rechargeable implements Initializable {

    @FXML
    private FlowPane fllowPane;
    private UsuarioDto usuarioDto;
    private ObservableList<DirectorioDto> directorios;
    private Transicion trans = new Transicion();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuarioDto = (UsuarioDto) AppContext.getInstance().get("UsuarioDto");
        cargarListaDeVersiones();
    }

    @FXML
    private void close(ActionEvent event) {
        TbxControl.getInstance().eliminarLoader("ControladorVersiones");
        ((Stage) fllowPane.getScene().getWindow()).close();

    }

    public void cargarListaDeVersiones() {
        //TODO
        fllowPane.getChildren().clear();
        directorios = (ObservableList<DirectorioDto>) ControlDirectorios.getInstance().listarCarpetasBusarVersiones(usuarioDto);
        for (DirectorioDto directorio : directorios) {
            fllowPane.getChildren().add(directorio);
            trans.cargarArchivosAndDirectorios(directorio);
        }
        activarEventos();
    }

    /**
     * Restaura version
     *
     * @param path direccion del directorio donde se ubica la copia
     */
    public void restaurarCopia(String path) {
        for (DirectorioDto directorio : directorios) {
            if (directorio.getDirLocation().equalsIgnoreCase(path)) {
                AppContext.getInstance().set("DirectorioOtros", directorio);
                TbxControl.getInstance().eliminarLoader("ControladorVersiones");
                
            }
        }
    }

    public void activarEventos() {
        for (Node node : this.fllowPane.getChildren()) {
            if (node instanceof DirectorioDto) {
                DirectorioDto dir = (DirectorioDto) node;
                ContextMenu context = new ContextMenu();
                MenuItem abrir = new MenuItem("Restaurar archivos hasta este punto en el tiempo");
                context.getItems().addAll(abrir);
                abrir.setOnAction(event -> restaurarCopia(dir.getDirLocation()));
                dir.setOnMouseClicked(event -> context.show(dir, event.getScreenX(), event.getScreenY()));
            }
        }
    }

    @Override
    public void reOpen() {
    }

}
