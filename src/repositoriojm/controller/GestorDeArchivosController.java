/* 
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates 
 * and open the template in the editor. 
 */
package repositoriojm.controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Stack;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import repositoriojm.model.Archivo;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.PermisoDto;
import repositoriojm.model.UsuarioDto;
import repositoriojm.services.UsuarioService;
import repositoriojm.util.AppContext;
import repositoriojm.util.ControlDirectorios;
import repositoriojm.util.MensajePopUp;
import repositoriojm.util.Respuesta;
import repositoriojm.util.TbxControl;
import repositoriojm.util.Transicion;

/**
 * FXML Controller class
 *
 * @author rober
 */
public class GestorDeArchivosController extends Rechargeable implements Initializable {

    private DirectorioDto direc;
    @FXML
    private FlowPane flowRoot;
    @FXML
    private Button btnBack;
    @FXML
    private Button btnFront;
    @FXML
    private TextField txt_Direccion;
    private ObservableList<Archivo> achiList;
    private ObservableList<DirectorioDto> directiList;
    private final Stack<String> pilaDireciones = new Stack();
    private UsuarioDto usuarioDto;
    private DirectorioDto dirPermanente;
    private DirectorioDto dirTemporal;
    private DirectorioDto directorioActual;
    private final MensajePopUp msjP = new MensajePopUp();
    private final Transicion trans = new Transicion();
    private Boolean soloEnTemporal = false;
    private Boolean esteSi = false;
    @FXML
    private Button btnVersiones;

    public GestorDeArchivosController() {
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuarioDto = (UsuarioDto) AppContext.getInstance().get("UsuarioDto");
        AppContext.getInstance().set("GestorDeArchivosController", this);
        esteSi = true;
    }

    @Override
    public void reOpen() {
    }

     @FXML
    private void pull(ActionEvent event) {

        Platform.runLater(() -> {
            flowRoot.getChildren().clear();
            Integer index = usuarioDto.getDirectorios().size();
            if (!ControlDirectorios.getInstance().estaVasioDirectorio(usuarioDto.getDirectorios().get(0).getDirLocation())) {
                msjP.notifyMensajeInformacion("Un momento", "Los archivos se estan cargando, un momento por favor!!");
                ControlDirectorios.getInstance().moverCarpetaCarpetaRecursivo(usuarioDto.getDirectorios().get(index - 1).getDirLocation(), usuarioDto.getDirectorios().get(1).getDirLocation());
                ControlDirectorios.getInstance().listarArchivosSubCarpeta(usuarioDto.getDirectorios().get(1).getDirLocation());
                directiList = (ObservableList<DirectorioDto>) AppContext.getInstance().get("Directorios");
                achiList = (ObservableList<Archivo>) AppContext.getInstance().get("Archivos");
                for (DirectorioDto directorioDto : directiList) {
                    flowRoot.getChildren().add(directorioDto);
                    trans.cargarArchivosAndDirectorios(directorioDto);
                }
                for (Archivo archivo : achiList) {
                    flowRoot.getChildren().add(archivo);
                    trans.cargarArchivosAndDirectorios(archivo);
                }
                activarEventosDeClick();
            } else {
                msjP.notifyMensajeInformacion("Un momento", "Creando el primer directorio dentro de su repo!!");
                soloEnTemporal = true;
            }

        });

    }

    @FXML
    private void push(ActionEvent event) {
        msjP.notifyMensajeInformacion("Un momento", "Los archivos se estan guardando!!");

        Platform.runLater(() -> {
            java.util.Date fecha = new Date();
            String nombre = fecha.toString();
            nombre = nombre.replace(":", "-");

            UsuarioService use = new UsuarioService();
            DirectorioDto dir = new DirectorioDto();
            if (direc == null) {
                ControlDirectorios.getInstance().moverCarpetaCarpetaRecursivo2(usuarioDto.getDirectorios().get(1).getDirLocation(), usuarioDto.getDirectorios().get(0).getDirLocation() + "/" + nombre + "_" + usuarioDto.getUsuario());
                dir.setDirLocation( usuarioDto.getDirectorios().get(0).getDirLocation() + "/" + nombre + "_" + usuarioDto.getUsuario());
                usuarioDto.getDirectorios().add(dir);
                PermisoDto permisoDto = new PermisoDto();
                permisoDto.setPermtipo("Total");
                usuarioDto.getPermisos().add(permisoDto);
                direc = null;
                use.guardar_ModificarUsario(usuarioDto);
                ControlDirectorios.getInstance().eliminarArhivo(usuarioDto.getDirectorios().get(1).getDirLocation());
                ControlDirectorios.getInstance().crearUnaCarpetaRepo(usuarioDto.getNombre());
                flowRoot.getChildren().clear();
            } else {
                //actualizar directorio nada mas
                ControlDirectorios.getInstance().eliminarArhivo(direc.getDirLocation());
                ControlDirectorios.getInstance().moverCarpetaCarpetaRecursivo2(usuarioDto.getDirectorios().get(1).getDirLocation(), direc.getDirLocation());
                direc = null;
            }

        });

    }

    public void cargarDatos() {

        Platform.runLater(() -> {
            //ControlDirectorios.getInstance().moverCarpetaCarpetaRecursivo(usuarioDto.getDirectorios().get(0).getDirLocation(), usuarioDto.getDirectorios().get(1).getDirLocation());
            ControlDirectorios.getInstance().listarCarpetas(usuarioDto);
            usuarioDto.getDirectorios().forEach((t1) -> {

                flowRoot.getChildren().add(t1);
                activarEventosDeClick();
            });
        });

    }

    public void activarEventosDeClick() {
        flowRoot.getChildren().forEach(node -> {
            if (node instanceof Archivo) {
                Archivo arch = (Archivo) node;
                MenuItem abrir = new MenuItem("Abrir");
                //abrir.setDisable(obtenerPermisosDeDirectorio(directorioActual));
                abrir.setOnAction(event -> open(arch.getRuta()));
                MenuItem eliminar = new MenuItem("Eliminar");
                //eliminar.setDisable(obtenerPermisosDeDirectorio(directorioActual));
                eliminar.setOnAction(event -> eliminar(arch.getRuta()));
                ContextMenu context = new ContextMenu();
                context.getItems().addAll(abrir, eliminar);
                arch.setOnMouseClicked(event -> context.show(arch, event.getScreenX(), event.getScreenY()));
            } else if (node instanceof DirectorioDto) {
                DirectorioDto dir = (DirectorioDto) node;
                MenuItem abrir = new MenuItem("Abrir");
                abrir.setOnAction(event -> navigate(dir.getDirLocation(), true));
                MenuItem eliminar = new MenuItem("Eliminar");
                //eliminar.setDisable(obtenerPermisosDeDirectorio(dir) || pilaDireciones.isEmpty());
                ContextMenu context = new ContextMenu();
                context.getItems().addAll(abrir, eliminar);
                dir.setOnMouseClicked(event -> context.show(dir, event.getScreenX(), event.getScreenY()));
            }
        });
    }

    private void eliminar(String path) {
        ControlDirectorios.getInstance().eliminarArhivo(path);
        push(new ActionEvent());
        cosasBuenas();
    }

    private void open(String ruta) {

        try {
            File file = new File(ruta);
            Desktop.getDesktop().open(file);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(GestorDeArchivosController.class.getName()).log(Level.SEVERE, "Abriendo archivo", ex);
        }

    }

    private void navigate(String path, boolean gauradarEnPila) {
        if (gauradarEnPila) {
            pilaDireciones.push(path);
        }
        this.txt_Direccion.setText(path);
        flowRoot.getChildren().clear();
        ControlDirectorios.getInstance().listarArchivosSubCarpeta(path);
        directiList = (ObservableList<DirectorioDto>) AppContext.getInstance().get("Directorios");
        achiList = (ObservableList<Archivo>) AppContext.getInstance().get("Archivos");
        for (DirectorioDto directorioDto : directiList) {
            flowRoot.getChildren().add(directorioDto);
            trans.cargarArchivosAndDirectorios(directorioDto);
        }
        for (Archivo archivo : achiList) {
            flowRoot.getChildren().add(archivo);
            trans.cargarArchivosAndDirectorios(archivo);

        }
        activarEventosDeClick();
    }

    @FXML
    private void goBack(ActionEvent event) {
        if (!pilaDireciones.isEmpty()) {
            navigate(pilaDireciones.pop(), false);
        } else {
            msjP.notifyMensajeInformacion(":)", "No se puede retroceder más");
        }
    }

    @FXML
    private void goHome(ActionEvent event) {
        Integer index = usuarioDto.getDirectorios().size();
//        ControlDirectorios.getInstance().moverCarpetaCarpetaRecursivo(usuarioDto.getDirectorios().get(index - 1).getDirLocation(), usuarioDto.getDirectorios().get(1).getDirLocation());
        if (!ControlDirectorios.getInstance().estaVasio(usuarioDto.getDirectorios().get(1).getDirLocation())) {
            flowRoot.getChildren().clear();
            ControlDirectorios.getInstance().listarArchivosSubCarpeta(usuarioDto.getDirectorios().get(1).getDirLocation());
            directiList = (ObservableList<DirectorioDto>) AppContext.getInstance().get("Directorios");
            achiList = (ObservableList<Archivo>) AppContext.getInstance().get("Archivos");
            for (DirectorioDto directorioDto : directiList) {
                flowRoot.getChildren().add(directorioDto);
                trans.cargarArchivosAndDirectorios(directorioDto);
            }
            for (Archivo archivo : achiList) {
                flowRoot.getChildren().add(archivo);
                trans.cargarArchivosAndDirectorios(archivo);

            }
            activarEventosDeClick();
        } else {
            msjP.notifyMensajeInformacion("Aviso", "Primero debe hace pull!!");
        }

    }

    public void commitForzado() {
        if (!ControlDirectorios.getInstance().estaVasio(usuarioDto.getDirectorios().get(1).getDirLocation()) && !ControlDirectorios.getInstance().estaVasioDirectorio(usuarioDto.getDirectorios().get(1).getDirLocation())) {
            flowRoot.getChildren().clear();
            Platform.runLater(() -> {
                java.util.Date fecha = new Date();
                String nombre = fecha.toString();
                nombre = nombre.replace(":", "-");
                ControlDirectorios.getInstance().moverCarpetaCarpetaRecursivo2(usuarioDto.getDirectorios().get(1).getDirLocation(), usuarioDto.getDirectorios().get(0).getDirLocation() + "/" + nombre+ "_" + usuarioDto.getUsuario());
                UsuarioService use = new UsuarioService();
                DirectorioDto dir = new DirectorioDto();
                dir.setDirLocation(usuarioDto.getDirectorios().get(0).getDirLocation() + "/" + nombre+ "_" + usuarioDto.getUsuario());
                usuarioDto.getDirectorios().add(dir);
                PermisoDto permisoDto = new PermisoDto();
                permisoDto.setPermtipo("Total");
                usuarioDto.getPermisos().add(permisoDto);
                use.guardar_ModificarUsario(usuarioDto);
                ControlDirectorios.getInstance().eliminarArhivo(usuarioDto.getDirectorios().get(1).getDirLocation()+"/");
                ControlDirectorios.getInstance().crearUnaCarpetaRepo(usuarioDto.getNombre());
            });
        }
    }

    public Boolean getEsteSi() {
        return esteSi;
    }

    public void setEsteSi(Boolean esteSi) {
        this.esteSi = esteSi;
    }

    @FXML
    private void verOtrosRepositorios(ActionEvent event) {

        TbxControl.getInstance().goViewInWindowModal("Repositorios", null, true, "Directorios compartidos", null);

        lindasCosas();

    }

    @FXML
    private void versiones(ActionEvent event) {
        TbxControl.getInstance().goViewInWindowModal("ControladorVersiones", null, true, "Versiones", null);
        lindasCosas();
    }

    private void lindasCosas() {

        direc = (DirectorioDto) AppContext.getInstance().get("DirectorioOtros");
        Platform.runLater(() -> {
            if (direc != null) {

                flowRoot.getChildren().clear();
                Integer index = usuarioDto.getDirectorios().size();
                if (!ControlDirectorios.getInstance().estaVasioDirectorio(direc.getDirLocation())) {

                    msjP.notifyMensajeInformacion("Un momento", "Los archivos se estan cargando, un momento por favor!!");
                    ControlDirectorios.getInstance().moverCarpetaCarpetaRecursivo(direc.getDirLocation(), usuarioDto.getDirectorios().get(1).getDirLocation());
                    ControlDirectorios.getInstance().listarArchivosSubCarpeta(usuarioDto.getDirectorios().get(1).getDirLocation());
                    directiList = (ObservableList<DirectorioDto>) AppContext.getInstance().get("Directorios");
                    achiList = (ObservableList<Archivo>) AppContext.getInstance().get("Archivos");
                    for (DirectorioDto directorioDto : directiList) {
                        flowRoot.getChildren().add(directorioDto);
                        trans.cargarArchivosAndDirectorios(directorioDto);
                    }
                    for (Archivo archivo : achiList) {
                        flowRoot.getChildren().add(archivo);
                        trans.cargarArchivosAndDirectorios(archivo);
                    }

                    activarEventosDeClick();
                }
            }
            cosasBuenas();
        });
    }

    private void cosasBuenas() {
        UsuarioService usuarioService = new UsuarioService();
        Respuesta respuesta = usuarioService.getUsuario(usuarioDto.getUsuario(), usuarioDto.getContrasenna());
        if (respuesta.getEstado()) {
            UsuarioDto usu = (UsuarioDto) respuesta.getResultado("UsuarioDto");
            AppContext.getInstance().set("UsuarioDto", usu);
            usuarioDto = usu;
        }
        goHome(new ActionEvent());
    }
}
