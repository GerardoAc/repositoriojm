/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.UsuarioDto;
import repositoriojm.services.UsuarioService;
import repositoriojm.util.AppContext;
import repositoriojm.util.ControlDirectorios;
import repositoriojm.util.MensajePopUp;
import repositoriojm.util.Respuesta;
import repositoriojm.util.TbxControl;
import repositoriojm.util.Transicion;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class GestionPermisosController extends Rechargeable implements Initializable {

    @FXML
    private FlowPane flowPaneArchivos;

    private UsuarioDto usuarioDto;

    private UsuarioDto usu;

    private ObservableList<DirectorioDto> directorios;

    private Transicion trans = new Transicion();
    @FXML
    private TableView<UsuarioDto> tvUsuarios;
    @FXML
    private TableColumn<UsuarioDto, String> tcNombre;
    @FXML
    private TableColumn<UsuarioDto, String> tcUsuario;

    private MensajePopUp msjP = new MensajePopUp();

    ObservableList<UsuarioDto> usuariosDto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuarioDto = (UsuarioDto) AppContext.getInstance().get("UsuarioDto");
        cargarArhivos();
    }

    @Override
    public void reOpen() {
    }

    public void cargarArhivos() {
        directorios = (ObservableList<DirectorioDto>) ControlDirectorios.getInstance().listarCarpetasAdmin(usuarioDto.getDirectorios().get(0).getDirLocation());
        for (DirectorioDto directorio : directorios) {
            flowPaneArchivos.getChildren().add(directorio);
            trans.cargarArchivosAndDirectorios(directorio);
        }
        cargarUsuarios();
        eventoTabla();

    }

    private void eventoTabla() {
        tvUsuarios.setOnMouseClicked((event) -> {
            usu = tvUsuarios.getSelectionModel().getSelectedItem();

        });
        cargarEventoArchivos();
    }

    private void cargarEventoArchivos() {
        for (DirectorioDto directorio : directorios) {
            directorio.setOnMouseClicked((event) -> {
                if (usu != null) {
                    AppContext.getInstance().set("Usuario", usu);
                    AppContext.getInstance().set("Directorio", (DirectorioDto) event.getSource());
                    TbxControl.getInstance().viewInWindow("DarPermiso", null, "Dar permisos", true);
                } else {
                    msjP.notifyMensajeError("Falta", "Debe seleccionar un usuario");
                }

            });
        }
    }

    private void cargarUsuarios() {
        try {

            UsuarioService usuarioService = new UsuarioService();
            Respuesta respuesta = usuarioService.getUsuarioList();
            if (respuesta.getEstado()) {
                usuariosDto = (ObservableList<UsuarioDto>) respuesta.getResultado("UsuariosList");
                usuariosDto.remove(0);
                tcNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
                tcUsuario.setCellValueFactory(new PropertyValueFactory<>("usuario"));
                tvUsuarios.setItems(usuariosDto);
            } else {
                msjP.notifyMensajeError("Cargando Datos", respuesta.getMensaje());

            }

        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, "Error cargando.", ex);
        }
    }

}
