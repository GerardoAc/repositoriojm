/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.controller;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import repositoriojm.model.UsuarioDto;
import repositoriojm.services.UsuarioService;
import repositoriojm.util.AppContext;
import repositoriojm.util.ControlDirectorios;
import repositoriojm.util.MensajePopUp;
import repositoriojm.util.Respuesta;
import repositoriojm.util.TbxControl;

/**
 * FXML Controller class
 *
 * @author LordLalo
 */
public class LoginController extends Rechargeable implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private TextField txtUsuario;
    @FXML
    private TextField txtClave;

    private MensajePopUp msjP = new MensajePopUp();
    @FXML
    private JFXButton btnLogin;
    @FXML
    private JFXButton btnRegisto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // new UsuarioService().getUsuario("u", "u");
    }

    @Override
    public void reOpen() {

    }

    @FXML
    private void login(ActionEvent event) throws IOException {
        try {

            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                msjP.notifyMensajeError("Validación de usuario", " Es necesario digitar un usuario para ingresar al sistema.");
            } else if (txtClave.getText() == null || txtClave.getText().isEmpty()) {
                msjP.notifyMensajeError("Validación de clave", " Es necesario digitar una clave para ingresar al sistema.");
            } else {
                UsuarioService usuarioService = new UsuarioService();
                Respuesta respuesta = usuarioService.getUsuario(txtUsuario.getText(), txtClave.getText());
                if (respuesta.getEstado()) {
                    UsuarioDto usu = (UsuarioDto) respuesta.getResultado("UsuarioDto");
                    AppContext.getInstance().set("UsuarioDto", usu);
                    if (usu.getAdministrador()) {
                         TbxControl.getInstance().view("GestionPermisos");   
                    }else{
                        TbxControl.getInstance().view("GestorDeArchivos");   
                    }
                    
                } else {
                    msjP.notifyMensajeError("Ingreso", respuesta.getMensaje());
                  
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }
    }

    @FXML
    private void registrar(ActionEvent event) {
        TbxControl.getInstance().view("Registro");
        
    }

}
