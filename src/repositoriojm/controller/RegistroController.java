/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.PermisoDto;
import repositoriojm.model.UsuarioDto;
import repositoriojm.services.UsuarioService;
import repositoriojm.util.AppContext;
import repositoriojm.util.ControlDirectorios;
import repositoriojm.util.MensajePopUp;
import repositoriojm.util.Respuesta;
import repositoriojm.util.TbxControl;
import repositoriojm.util.Respuesta;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class RegistroController extends Rechargeable implements Initializable {

    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXTextField txtClave;
    @FXML
    private JFXButton btnRegistro;
    @FXML
    private JFXButton btnVolver;

    private MensajePopUp msjP = new MensajePopUp();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @Override
    public void reOpen() {
    }

    @FXML
    private void registro(ActionEvent event) {
        try {

            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                msjP.notifyMensajeError("Validación de usuario", " Es necesario digitar un usuario para ingresar al sistema.");
            } else if (txtClave.getText() == null || txtClave.getText().isEmpty()) {
                msjP.notifyMensajeError("Validación de clave", " Es necesario digitar una clave para ingresar al sistema.");
            } else if (txtNombre.getText() == null || txtNombre.getText().isEmpty()) {
                msjP.notifyMensajeError("Validación de clave", " Es necesario digitar una clave para ingresar al sistema.");
            } else {
                UsuarioService usuarioService = new UsuarioService();
                UsuarioDto usuarioDto = new UsuarioDto();
                usuarioDto.setNombre(txtNombre.getText());
                usuarioDto.setUsuario(txtUsuario.getText());
                usuarioDto.setContrasenna(txtClave.getText());
                //generacion de directorios temporal y permanente
                List<String> rutas = ControlDirectorios.getInstance().crearCarpetasRepo(usuarioDto.getNombre());

                DirectorioDto dir1 = new DirectorioDto();
                dir1.setDirLocation(rutas.get(0));
                PermisoDto permi1 = new PermisoDto();
                permi1.setPermtipo("Total");

                DirectorioDto dir2 = new DirectorioDto();
                dir2.setDirLocation(rutas.get(1));
                PermisoDto permi2 = new PermisoDto();
                permi2.setPermtipo("Total");

                usuarioDto.getDirectorios().add(dir1);
                usuarioDto.getDirectorios().add(dir2);

                usuarioDto.getPermisos().add(permi1);
                usuarioDto.getPermisos().add(permi1);

                Respuesta respuesta = usuarioService.guardar_ModificarUsario(usuarioDto);

                if (respuesta.getEstado()) {
                    AppContext.getInstance().set("UsuarioDto", (UsuarioDto) respuesta.getResultado("UsuarioDto"));
                    msjP.notifyMensajeInformacion("Registor", "El registro fue exitoso");
                    Thread.sleep(1000);
                    TbxControl.getInstance().view("Login");
                } else {
                    msjP.notifyMensajeError("Registor", respuesta.getMensaje());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
        }
    }

    @FXML
    private void volver(ActionEvent event) {
        TbxControl.getInstance().view("Login");
    }

}
