/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.UsuarioDto;
import repositoriojm.util.AppContext;
import repositoriojm.util.ControlDirectorios;
import repositoriojm.util.MensajePopUp;
import repositoriojm.util.TbxControl;
import repositoriojm.util.Transicion;

/**
 * FXML Controller class
 *
 * @author jocse
 */
public class RepositoriosController extends Rechargeable implements Initializable {

    /**
     * Initializes the controller class.
     */
    private UsuarioDto usuarioDto;
    @FXML
    private FlowPane flowPaneDirectorios;

    private MensajePopUp msjP = new MensajePopUp();

    ObservableList<UsuarioDto> usuariosDto;

    private Transicion trans = new Transicion();

    private ObservableList<DirectorioDto> directorios;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuarioDto = (UsuarioDto) AppContext.getInstance().get("UsuarioDto");
        cargarArhivos();
    }

    @Override
    public void reOpen() {

    }

    public void cargarArhivos() {
        directorios = (ObservableList<DirectorioDto>) ControlDirectorios.getInstance().listarCarpetasUsuario(usuarioDto);
        for (DirectorioDto directorio : directorios) {
            flowPaneDirectorios.getChildren().add(directorio);
            trans.cargarArchivosAndDirectorios(directorio);
        }
        cargarEventoDirectorio();
    }

    private void cargarEventoDirectorio() {
        for (DirectorioDto directorio : directorios) {
            directorio.setOnMouseClicked((event) -> {
                DirectorioDto dir = (DirectorioDto) event.getSource();
                AppContext.getInstance().set("DirectorioOtros", dir);
                ((Stage) flowPaneDirectorios.getScene().getWindow()).close();
            });
        }
    }
}
