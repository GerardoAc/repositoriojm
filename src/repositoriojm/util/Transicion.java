/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.util;

import com.jfoenix.controls.JFXButton;
import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 *
 * @author jocse
 */
public class Transicion {

    private ScaleTransition st;
    private ScaleTransition st2;
    private int n;

    public Transicion() {
        this.n = 1;
    }

    /**
     * desaparicion de la pantalla
     *
     * @param iv3
     */
    public void aniCamPantallaInicial(StackPane vBox) {
        FadeTransition ft = new FadeTransition(Duration.millis(100), vBox);
        ft.setDelay(Duration.seconds(1));
        ft.setFromValue(1);
        ft.setToValue(0);
        ft.play();

    }

    /**
     * Animacion de aparicion de la pantalla
     *
     * @param vBox
     */
    public void cargarArchivosAndDirectorios(JFXButton vBox) {
        FadeTransition ft2 = new FadeTransition(Duration.millis(700), vBox);
        ft2.setFromValue(0);
        ft2.setToValue(1);
        ft2.play();

    }

}
