/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositoriojm.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import repositoriojm.model.Archivo;
import repositoriojm.model.DirectorioDto;
import repositoriojm.model.PermisoDto;
import repositoriojm.model.UsuarioDto;

/**
 *
 * @author jocse
 */
public class ControlDirectorios {

    private static ControlDirectorios INSTANCE = null;
    private static HashMap<String, Object> context = new HashMap<>();
    private List<String> lisRutas = new ArrayList<>();
    private List<String> lisNombre = new ArrayList<>();
    private List<String> lisRutasDirectorios = new ArrayList<>();
    private List<String> lisNombreDirectorios = new ArrayList<>();

    private static void createInstance() {
        if (INSTANCE == null) {
            synchronized (AppContext.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ControlDirectorios();
                }
            }
        }
    }

    public static ControlDirectorios getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }

    /**
     * CreaCION DE carpeta especifica
     *
     * @param ruta donde se va a alojar la carpeta
     * @param nombreCarpeta nombre de la carpeta
     */
    public String crearCarpetaUnica(String ruta, String nombreCarpeta) {
        String rutaCarp = ruta + "/" + nombreCarpeta;
        File carpeta = new File(rutaCarp);
        if (!carpeta.exists()) {
            if (carpeta.mkdirs()) {
                System.out.println("Carpeta creada");
            } else {
                System.out.println("Error al crear directorio");
            }
        }
        return rutaCarp;
    }

    /**
     * Genera las caparpetas en el directorio raiz
     *
     * @param direccionTemporal
     * @param direccionPermanente
     */
    public List<String> crearCarpetasRepo(String nombreUsuario) {
        List<String> rutas = new ArrayList<>();

        String temporal = "raiz/directorioPermantente_" + nombreUsuario;
        String permanete = "raiz/directorioTemporal_" + nombreUsuario;
        File direcorioTemporal = new File(temporal);
        if (!direcorioTemporal.exists()) {
            if (direcorioTemporal.mkdirs()) {
                System.out.println("Directorio creado");
                rutas.add(temporal);
            } else {
                System.out.println("Error al crear directorio");
            }
        }
        File direcorioPermanete = new File(permanete);
        if (!direcorioPermanete.exists()) {
            if (direcorioPermanete.mkdirs()) {
                System.out.println("Directorio creado");
                rutas.add(permanete);
            } else {
                System.out.println("Error al crear directorio");
            }
        }
        return rutas;
    }

    public void crearUnaCarpetaRepo(String nombreUsuario) {

        String temporal = "raiz/directorioTemporal_" + nombreUsuario;
        File direcorioTemporal = new File(temporal);
        if (!direcorioTemporal.exists()) {
            if (direcorioTemporal.mkdirs()) {
                System.out.println("Directorio creado");

            } else {
                System.out.println("Error al crear directorio");
            }
        }

    }


    public String crearArchivoCarpeta(String rutaCarpeta, String nombreArchivo, String extencion) throws IOException {
        String rutaArchivo = rutaCarpeta + "/" + nombreArchivo + "." + extencion;
        File archivo = new File(rutaArchivo);
        BufferedWriter bw;
        if (archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write("El fichero de texto ya estaba creado.");
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write("Acabo de crear el fichero de texto.");
        }
        bw.close();
        return rutaArchivo;
    }

    public void listarCarpetas(UsuarioDto usuarioDto) {

        for (DirectorioDto directorio : usuarioDto.getDirectorios()) {
            ImageView img = new ImageView(new Image("repositoriojm/resources/dir.png"));
            img.setFitWidth(60);
            img.setFitHeight(60);
            directorio.setStyle("-fx-font-size: 10px;-fx-content-display: top;");
            directorio.setGraphic(img);
            File f = new File(directorio.getDirLocation());
            directorio.setText(f.getName());
        }

    }

    public void listarArchivosSubCarpeta(String rutaCarpeta) {
        ObservableList<Archivo> acr = FXCollections.observableArrayList();
        ObservableList<DirectorioDto> directorios = FXCollections.observableArrayList();
        File f = new File(rutaCarpeta);
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (File fichero : ficheros) {
                if (fichero.isFile()) {
                    Archivo a = new Archivo(fichero.getName(), fichero.getPath());
                    ImageView img = new ImageView(new Image("repositoriojm/resources/file.png"));
                    img.setFitWidth(60);
                    img.setFitHeight(60);
                    a.setStyle("-fx-font-size: 10px;-fx-content-display: top;");
                    a.setGraphic(img);
                    a.setText(fichero.getName());
                    acr.add(a);
                } else {
                    if (fichero.isDirectory() && carpertaTieneArchivos(fichero.getPath())) {
                        DirectorioDto dir = new DirectorioDto();
                        ImageView img = new ImageView(new Image("repositoriojm/resources/dir.png"));
                        img.setFitWidth(60);
                        img.setFitHeight(60);
                        dir.setStyle("-fx-font-size: 10px;-fx-content-display: top;");
                        dir.setGraphic(img);
                        dir.setDirLocation(fichero.getPath());
                        dir.setText(fichero.getName());
                        directorios.add(dir);
                    }
                }
            }
        }

        AppContext.getInstance().set("Archivos", acr);
        AppContext.getInstance().set("Directorios", directorios);

    }

    public void moverArchivo(String origen, String destino) {
        Path origenPath = FileSystems.getDefault().getPath(origen);
        Path destinoPath = FileSystems.getDefault().getPath(destino);

        try {
            Files.move(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(ControlDirectorios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void moverArchivoCar(String origen, String destino) {
        Path origenPath = FileSystems.getDefault().getPath(origen);
        Path destinoPath = FileSystems.getDefault().getPath(destino);

        try {
            Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(ControlDirectorios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void moverCarpetaCarpeta(String origen, String destino) {
        listarRutasAndNombres(origen);
        for (int i = 0; i < lisRutas.size(); i++) {
            moverArchivo(lisRutas.get(i), destino + "/" + lisNombre.get(i));
        }
        for (int i = 0; i < lisRutasDirectorios.size(); i++) {
            moverArchivo(lisRutasDirectorios.get(i), destino + "/" + lisNombreDirectorios.get(i));
        }

    }

//    public void moverCarpetasInicioRecursivo(String origen, String destino) {
//        listarRutasAndNombres(origen);
//        moverCarpetaCarpetaRecursivo(origen, destino);
//    }
    public void moverCarpetaCarpetaRecursivo(String origen, String destino) {

        moverDirectoriosR(origen, destino);
        //eliminarDirectoriosR(origen);
    }

    public void eliminarArhivo(String dirActual) {
        try {

            File f = new File(dirActual);
            if (f.exists()) {
                if (f.isFile()) {
                    f.delete();
                } else {
                    eliminarDirectoriosR(dirActual);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void moverCarpetaCarpetaRecursivo2(String origen, String destino) {
        generarCarpeta(destino);
        moverDirectoriosR(origen, destino);
        //eliminarDirectoriosR(origen);
    }

    public void eliminarDirectoriosR(String dirActual) {
        try {

            File f = new File(dirActual);
            if (f.exists()) {
                File[] ficheros = f.listFiles();
                for (File fichero : ficheros) {
                    if (fichero.isDirectory()) {
                        eliminarDirectoriosR(fichero.getPath() + "/");
                    } else {
                        fichero.delete();
                    }
                }
                f.delete();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public Boolean estaVasio(String dirActual) {
        File f = new File(dirActual);
        Integer elementos = 0;
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (File fichero : ficheros) {
                if (fichero.isFile()) {
                    elementos++;
                }
            }
        }
        if (elementos > 0) {
            return false;
        }
        return true;
    }

    public Boolean estaVasioDirectorio(String dirActual) {
        File f = new File(dirActual);
        Integer elementos = 0;
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (File fichero : ficheros) {
                if (fichero.isDirectory()) {
                    elementos++;
                } else {
                    elementos++;
                }
            }
        }
        if (elementos > 0) {
            return false;
        }
        return true;
    }

    public Boolean carpertaTieneArchivos(String dirActual) {
        File f = new File(dirActual);
        Integer elementos = 0;
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (File fichero : ficheros) {
                if (fichero.isFile()) {
                    elementos++;
                }
            }
        }
        if (elementos > 0) {
            return true;
        }
        return false;
    }

    private void moverDirectoriosR(String dirActual, String destino) {
        File f = new File(dirActual);
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (File fichero : ficheros) {
                if (fichero.isDirectory()) {
                    generarCarpeta(destino + "/" + fichero.getName());
                    moverDirectoriosR(fichero.getPath(), destino + "/" + fichero.getName());

                } else {
                    moverArchivoCar(fichero.getPath(), destino + "/" + fichero.getName());
                }
            }
        }
    }

    public void generarCarpeta(String destino) {
        File direcorioTemporal = new File(destino);
        if (!direcorioTemporal.exists()) {
            if (direcorioTemporal.mkdirs()) {
                System.out.println("Directorio creado");
            } else {
                System.out.println("Error al crear directorio");
            }
        }
    }

    public void moverCarpeta(String origen, String destino) {
        Path origenPath = FileSystems.getDefault().getPath(origen);
        Path destinoPath = FileSystems.getDefault().getPath(destino);

        try {
            Files.move(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            Logger.getLogger(ControlDirectorios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listarRutasAndNombres(String rutaCarpeta) {

        File f = new File(rutaCarpeta);
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (File fichero : ficheros) {
                if (fichero.isFile()) {
                    lisNombre.add(fichero.getName());
                    lisRutas.add(fichero.getPath());
                } else {
                    if (fichero.isDirectory()) {
                        lisNombreDirectorios.add(fichero.getName());
                        lisRutasDirectorios.add(fichero.getPath());
                    }
                }
            }
        }
    }

    public List<DirectorioDto> listarCarpetasAdmin(String rutaCarpeta) {
        ObservableList<DirectorioDto> directorios = FXCollections.observableArrayList();
        File f = new File(rutaCarpeta);
        if (f.exists()) {
            File[] ficheros = f.listFiles();
            for (File fichero : ficheros) {

                if (fichero.isDirectory() && buscarPalabraPermanente(fichero.getName())) {
                    DirectorioDto dir = new DirectorioDto();
                    ImageView img = new ImageView(new Image("repositoriojm/resources/dir.png"));
                    img.setFitWidth(60);
                    img.setFitHeight(60);
                    dir.setStyle("-fx-font-size: 10px;-fx-content-display: top;");
                    dir.setGraphic(img);
                    dir.setDirLocation(fichero.getPath());
                    dir.setText(fichero.getName());
                    directorios.add(dir);

                }
            }

        }
        return directorios;
    }

    public List<DirectorioDto> listarCarpetasUsuario(UsuarioDto usuario) {
        ObservableList<DirectorioDto> directorios = FXCollections.observableArrayList();

        for (int i = 0; i < usuario.getDirectorios().size(); i++) {

            if (tieneDirectoriosOtros(usuario.getPermisos().get(i))) {

                File f = new File(usuario.getDirectorios().get(i).getDirLocation());
                if (f.exists()) {

                    if (f.isDirectory()) {
                        DirectorioDto dir = new DirectorioDto();
                        ImageView img = new ImageView(new Image("repositoriojm/resources/dir.png"));
                        img.setFitWidth(60);
                        img.setFitHeight(60);
                        dir.setStyle("-fx-font-size: 10px;-fx-content-display: top;");
                        dir.setGraphic(img);
                        dir.setDirLocation(f.getPath());
                        dir.setText(f.getName());
                        directorios.add(dir);

                    }
                }
            }

        }
        return directorios;
    }

    public List<DirectorioDto> listarCarpetasBusarVersiones(UsuarioDto usuario) {
        ObservableList<DirectorioDto> directorios = FXCollections.observableArrayList();

        for (int i = 0; i < usuario.getDirectorios().size(); i++) {

            if (propios(usuario.getPermisos().get(i))&&i>1) {

                File f = new File(usuario.getDirectorios().get(i).getDirLocation());
                if (f.exists()) {
                    if (f.isDirectory()) {
                        DirectorioDto dir = new DirectorioDto();
                        ImageView img = new ImageView(new Image("repositoriojm/resources/dir.png"));
                        img.setFitWidth(60);
                        img.setFitHeight(60);
                        dir.setStyle("-fx-font-size: 10px;-fx-content-display: top;");
                        dir.setGraphic(img);
                        dir.setDirLocation(f.getPath());
                        dir.setText(f.getName());
                        directorios.add(dir);

                    }
                }
            }

        }
        return directorios;
    }

    private Boolean tieneDirectoriosOtros(PermisoDto p) {
        if (p.getPermtipo().indexOf("-otro") != -1) {
            return true;
        }
        return false;
    }

    private Boolean propios(PermisoDto p) {
        if (p.getPermtipo().indexOf("-otro") != -1) {
            return false;
        }
        return true;
    }

    private Boolean buscarPalabraPermanente(String p) {
        if (p.indexOf("directorioPermantente") != -1) {
            return true;
        }
        return false;
    }
}
